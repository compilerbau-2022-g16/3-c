#include "minako.h"
#include <stdlib.h>
#include <stdio.h>

typedef struct Token {
    int token;
} Token;

Token currentToken;
Token nextToken;

void error(const char msg[]) {
    fprintf(stderr, "%s", msg);
    exit(-1);
}

void eat(void) {
    if (nextToken.token) {
        currentToken.token = nextToken.token;
        nextToken.token = yylex();
    }
    else {
        currentToken.token = yylex();
        nextToken.token = yylex();
    }
}

int isToken(int t) {
    if (currentToken.token == t) {
        return 1;
    }
    return 0;
}

int isTokenAndEat(int t) {
    if (isToken(t)) {
        eat();
        return 1;
    }
    error("expect: unexpected symbol");
    return 0;
}

void type() {
    if (isToken(KW_BOOLEAN) || isToken(KW_FLOAT) || isToken(KW_INT) || isToken(KW_FLOAT)) {
        eat();
    }
    else {
        error("type");
    }
}

void functioncall() {
    eat();
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
}

void assignment();

void factor() {
    if (isToken(CONST_BOOLEAN) || isToken(CONST_FLOAT) || isToken(CONST_INT)) {
        eat();
    }
    else if (isToken(ID)) {
        eat();
        if (isToken('('))
            functioncall();
    }
    else if (isToken('(')) {
        assignment();
        isTokenAndEat(')');
    }
    else {
        error("factor");
    }
}

void term() {
    factor();
    while (isToken('*') || isToken('/') || isToken(AND)) {
        eat();
        factor();
    }
}

void simpexpr() {
    if (isToken('-')) {
        eat();
    }
    term();
    if (isToken('+') || isToken('-') || isToken(OR)) {
        eat();
        term();
    }

}

void expr() {
    eat();
    simpexpr();
    if (isToken(EQ) || isToken(NEQ) || isToken(LEQ) || isToken(GEQ) || isToken(GRT) || isToken(LSS)) {
        simpexpr();
    }
}

void assignment() {
    if (nextToken.token == '=') {
        isTokenAndEat(ID);
        eat();
        assignment();
    }
    else {
        expr();
    }
}

void statassignment() {
    isTokenAndEat(ID);
    isTokenAndEat('=');
    assignment();
}

void printfstatement() {
    eat();
    isTokenAndEat('(');
    assignment();
    isTokenAndEat(')');
}

void returnstatement() {
    eat();
    if (isToken('='))
        assignment();
}

void block();

void ifstatement() {
    eat();
    isTokenAndEat('(');
    statassignment();
    isTokenAndEat(')');
    block();
}

void statement() {
    if (isToken(KW_IF)) {
        ifstatement();
    }
    else if (isToken(KW_RETURN)) {
        returnstatement();
        isTokenAndEat(';');
    }
    else if (isToken(KW_PRINTF)) {
        printfstatement();
        isTokenAndEat(';');
    }
    else if (isToken(ID)) {
        eat();
        if (isToken('=')) {
            statassignment();
            isTokenAndEat(';');
        }
        else if (isToken('(')) {
            functioncall();
            isTokenAndEat(';');
        }
    }
    else if (isToken('}')) {
        return;
    }
    else {
        error("statement");
    }
}

int v;

void statementlist() {
    block();
    if (!v)
        statementlist();
}

void block() {
    eat();
    if (isToken('{')) {
        statementlist();
        isTokenAndEat('}');
        v = 0;
    }
    else {
        v = 1;
        statement();
    }

}

void functiondefinition(void) {
    type();
    isTokenAndEat(ID);
    isTokenAndEat('(');
    if (!isToken(')')) {
        while(!isToken(')')) {
            isTokenAndEat(ID);
            if (isToken(')')) {
                eat();
                break;
            }
            else if (currentToken.token != ',') {
                error(",");
            }
            eat();
        }
    }
    else {
        eat();
    }
    isTokenAndEat('{');
    statementlist();
    isTokenAndEat('}');
}

void program(void) {
    eat();
    if (currentToken.token == EOF) {
        return;
    }
    else {
        functiondefinition();

        program();
    }
}

int main(int argc, char** argv) {
    if (argc == 2) {
        yyin = fopen(argv[1], "r");
        if (yyin == 0) {
            error("Fehler: Datei fehlt!");
        }
    }
    else {
        yyin = stdin;
    }

    program();

    return 0;
}